package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}